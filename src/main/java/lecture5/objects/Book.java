package lecture5.objects;

public class Book {
	//feltvariabler
	String title;
	String author;
	
	public Book(String title, String author){
		this.title = title;
		this.author = author;
	}
	
	@Override
	public String toString() {
		return title+" by "+author;
	}
	
}
